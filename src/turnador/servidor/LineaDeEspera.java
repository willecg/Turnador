/* Turnador: LineaDeEspera.java
 * Sirve una línea de espera recibe conexiones, entrega y genera turnos.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.servidor;

import turnador.config.Linea;
import turnador.Registros;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Iterator;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

/**Maneja de forma individual las líneas de espera.
 * Control de registros en la bitácora, control de conexiones, retransmisión
 * de mensajes a clientes, envío de mensajes a sokets clientes, sirve turnos a
 * clientes.*/
public class LineaDeEspera extends Linea
{
    private Registros bitacora;
    /* Subprocesamiento múltiple.*/
    private ExecutorService ejecutor;
    /* Relaciones */
    private ConcurrentLinkedQueue<Turno> turnos;
    private ArrayList<Conexion> conexiones;
    
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *                                                                           *
 *                            Funciones miembro                              *
 *                                                                           *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /* Constructores --------------------------------------------------------*/
    /**Constructor por defecto.
     * Crea una nueva línea de espera.
     * @throws FileNotFoundException Archivo de bitácora no encontrado.
     * @throws IOException Error al escribir o leer el archivo de bitácora.*/
    public LineaDeEspera() throws FileNotFoundException, IOException
    {
        this("/var/log/turnadores/queues/");
    }

    /**Constructor con un argumento.
     * Crea una nueva línea de espera indicando cual es el archivo de bitácora
     * que se desea establecer.
     * @param bitacora Cadena con ruta y nombre del archivo ó bitácora.
     * @throws FileNotFoundException Archivo de bitácora no encontrado.
     * @throws IOException Error al escribir o leer el archivo de bitácora.*/
    public LineaDeEspera(String bitacora)
        throws FileNotFoundException, IOException
    {
        this(bitacora, new Linea());
    }

    /**Constructor completo.
     * Crea una nueva línea de espera indicando cual es el archivo de bitácora
     * que se desea establecer y la línea obtenida desde el archivo de
     * configuración.
     * @param bitacora Cadena con ruta y nombre del archivo ó bitácora.
     * @param linea Linea que serve para los atributos de la clase padre.
     * @throws FileNotFoundException Archivo de bitácora no encontrado.
     * @throws IOException Error al escribir o leer el archivo de bitácora.*/
    public LineaDeEspera(String bitacora, Linea linea)
        throws FileNotFoundException, IOException
    {
        super();
        setBitacora(linea.getLinea(), bitacora);
        setLinea(linea.getLinea());
        setTurno(linea.getTurno());
        setNombre(linea.getNombre());
        this.bitacora.registrarBitacora(", Linea de espera " + getLinea() +
                " Iniciada", true);
        turnos = new ConcurrentLinkedQueue<Turno>();
        conexiones = new ArrayList<Conexion>();
        ejecutor = Executors.newCachedThreadPool();
    }

    /**Elimina un elemento del arreglo de conexiones.
     * @param conexion Referencia al objeto a eliminar. */
    public void eliminarConexion(Conexion conexion)
    {
        Iterator<Conexion> iterador = conexiones.iterator();
        while(iterador.hasNext())
            if(iterador.next() == conexion)
            {
                iterador.remove();
                break;
            }
    }

    /**Cierra la línea de espera.
     * Desconecta a todos los clientes y ordena el cierre del archivo de
     * bitácora.*/
    public void terminar() throws IOException
    {
        Iterator<Conexion> iterador = conexiones.iterator();
        Conexion conexion;
        enviarMensajeMasivo("FINALIZADA");
        while(iterador.hasNext())
        {
            conexion = iterador.next();
         //   conexion.enviarMensaje("FINALIZADA");
            iterador.remove();
        }
        bitacora.cerrar();
    }

    public int generarTurno()
    {
        Turno turno = new Turno(getTurno());
        setTurno(getTurno() + 1);
        turnos.add(turno);
        return turno.getNoTurno();
    }

    public int obtenerTurno(int ventanilla)
    {
        SimpleDateFormat formato = new SimpleDateFormat("d/M/y-H:m:s");
        if(!turnos.isEmpty())
        {
            Turno turno = turnos.poll();
            turno.setVentanilla(ventanilla);
            Calendar salida = turno.getFechaFin();
            Calendar entrada = turno.getFechaInicio();
            String registro = formato.format(entrada.getTime()) + ", " +
                formato.format(salida.getTime()) + ", " +
                turno.getVentanilla() + ", " + turno.getNoTurno();
            bitacora.registrarBitacora(registro, false);
            return turno.getNoTurno();
        }
        return -1;
    }

    public void enviarMensajeMasivo(String mensaje)
    {
        Iterator<Conexion> iter = conexiones.iterator();
        while(iter.hasNext())
        {
            iter.next().enviarMensaje(mensaje);
        }
    }

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *                                                                           *
 *                           Funciones SET & GET                             *
 *                                                                           *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /* Set ------------------------------------------------------------------*/
    /**Establece la bitácora.
     * Establece el nombre del archivo de bitácora, por medio de el índice de
     * la línea de espera y de la ruta destino.
     * @param index Indice de la bitácora.
     * @param bitacora Ruta la carpeta donde se encuentran las bitácoras de las
     * líneas de espera.
     * @throws FileNotFoundException Archivo o carpeta no encontrado.
     * @throws IOException Error de escritura o de lectura.*/
    public void setBitacora(int index, String bitacora)
       throws FileNotFoundException, IOException
    {
        bitacora = bitacora + "queue_" + index + ".log";
        this.bitacora = new Registros(bitacora);
    }

    public void addConexion(Conexion conexion)
    {
        conexion.addOwner(this);
        String msg = "";
        if(turnos.isEmpty())
            msg = "NOHAYTURNOS";
        else
        {
            Turno turno = turnos.peek();
            msg = getNombre() + " " + turno.getVentanilla() +
                " TURNO " + turno.getNoTurno();
        }
        conexion.enviarMensaje(msg);
        conexiones.add(conexion);
        ejecutor.execute(conexion);
    }
}
