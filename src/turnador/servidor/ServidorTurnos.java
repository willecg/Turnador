/*
 * Turnador: ServidorTurnador.java
 * Maneja las conexiones de red e inicia/cierra la aplicación.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gómez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.servidor;

import turnador.config.Arbol;
import turnador.config.Configuracion;
import turnador.config.Linea;
import turnador.config.ParserException;
import turnador.Registros;

import java.util.ArrayList;
import java.util.Iterator;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

/**Clase principal para servir los turnos.
 * Prepara los hilos de ejecución para cada uno de los clientes, abre el
 * socket servidor y maneja cada una de las conexiones que se establecen. */
public class ServidorTurnos extends Arbol
{
    private ArrayList<LineaDeEspera> lineasDeEspera;
    private Registros registros;   
    private ServerSocket socket; // Socket servidor.
    
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                       *
     *                          Constructores                                *
     *                                                                       *
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**Constructor por defecto.
     * @throws FileNotFoundException Archivo no encontrado.
     * @throws IOException Error de entrada/salida del archivo.*/
    public ServidorTurnos()
        throws FileNotFoundException, IOException, ClassNotFoundException
    {
        this(new Arbol(), "/var/log/turnadores/");
    }

    /**Constructor indicando el arbol para el servidor.
     * @param arbol Arbol de quien se tomaran los datos para el padre.
     * @throws FileNotFoundException Archivo no encontrado.
     * @throws IOException Error de entrada/salida del archivo.*/
    public ServidorTurnos(Arbol arbol)
        throws FileNotFoundException, IOException, ClassNotFoundException
    {
        this(arbol, "/var/log/turnadores/");
    }

    /**Constructor indicando el arbol y el archivo de bitácora.
     * @param arbol Arbol de quien se tomaran los datos para el padre.
     * @param logDir Archivo que se desa usar como bitácora.
     * @throws FileNotFoundException Archivo no encontrado.
     * @throws IOException Error de entrada/salida del archivo.*/
    public ServidorTurnos(Arbol arbol, String logDir)
        throws FileNotFoundException, IOException, ClassNotFoundException
    {
        /* Preparación del servidor. */
        super();
        setRegistros(logDir);
        registros.registrarBitacora("Servidor iniciado.", true);
        setServidor_id(arbol.getServidor_id());
        setPuerto(arbol.getPuerto());
        setMaximoconexiones(arbol.getMaximoconexiones());
        
        /* Creación de las líneas de espera. */
        Iterator <Linea> linea = arbol.getIterLinea();
        lineasDeEspera = new ArrayList<LineaDeEspera>();
        while(linea.hasNext())
        {   
            String bitacora = logDir + "queues/";
            lineasDeEspera.add(new LineaDeEspera(bitacora, linea.next()));
        }

        /* Arranque del servidor. */
        String mensaje = "CONTINUAR";
        socket = new ServerSocket(getPuerto(), getMaximoconexiones());
        while(!mensaje.equals("TERMINARSERVIDOR"))
        {
            Conexion con = new Conexion(socket);
            con.obtenerFlujos();
            con.enviarMensaje("SOLICITARDATOS");
            mensaje = con.procesarMensajes();
            if(!mensaje.equals("TERMINARSERVIDOR"))
            {
                int id_linea = Integer.parseInt(mensaje);
                Iterator<LineaDeEspera> iter = lineasDeEspera.iterator();
                LineaDeEspera linea_de_espera;
                boolean lineaEncontrada = false;
                while(iter.hasNext())
                {
                    linea_de_espera = iter.next();
                    if(id_linea == linea_de_espera.getLinea())
                    {
                        linea_de_espera.addConexion(con);
                        lineaEncontrada = true;
                        break;
                    } 
                } // Fin del while interno
                if(!lineaEncontrada)
                    con.cerrarConexion();
            } // Fin del if externo
        } // Fin del while mas externo
    }

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                       *
     *                         Funciones Operativas                          *
     *                                                                       *
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**Detiene el servidor.
     * @throws IOException Error de entrada/salida.*/
    public void detener() throws IOException
    {
        Iterator<LineaDeEspera> iter = lineasDeEspera.iterator();
        LineaDeEspera linea;
        while(iter.hasNext())
        {
            linea = iter.next();
            linea.terminar();
            iter.remove();
        }
        registros.registrarBitacora("Servidor detenido.", true);
        registros.cerrar();
    }

    /**Envía la señal de apagado al servidor.
     * @param arbol Arbol sintactico con los valores del servidor.
     * @throws IOException Errores en el envío de información.
     * @throws ClassNotFoundException Clase no encontrada.*/
    public static void apagarServidor(Arbol arbol)
        throws IOException, ClassNotFoundException
    {
        Socket cliente = new Socket(
                InetAddress.getByName("127.0.0.1"), arbol.getPuerto());
        ObjectOutputStream salida = 
            new ObjectOutputStream(cliente.getOutputStream());
        ObjectInputStream entrada =
            new ObjectInputStream(cliente.getInputStream());
        String mensaje_entrada = (String) entrada.readObject();
        salida.flush();
        salida.writeObject("TERMINARSERVIDOR");
        salida.flush();
    }
    
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                       *
     *                         Funciones Get y Set                           *
     *                                                                       *
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /*++++++++++++++++++++++++ Funciones Set ++++++++++++++++++++++++++++++++*/
    /**Establece el archivo de registro.
     * @param uri Ruta al archivo junto al nombre del archivo.
     * @throws FileNotFoundException Archivo no encontrado.
     * @throws IOException Error de entrada/salida.*/
    public void setRegistros(String uri)
        throws FileNotFoundException, IOException
    {
        registros = new Registros(uri + "Turnador.log");
    }
}
