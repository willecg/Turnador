/*
 * Turnador: Turno.java
 * Operaciones sobre los turnos o tickets que se mostrarán en pantalla.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gómez
 *                                      <sistemas.will@alianzacpa.com.mx>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.servidor;

import java.util.Calendar;
/**
 * Clase singleton que ejecuta directamente al turnador por medio del
 * método nuevo(). */
public class Turno
{
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                       *
     *                      Atributos de la clase                            *
     *                                                                       *
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    private int noTurno;
    private Calendar fechaInicio;
    private Calendar fechaFin;
    private int ventanilla;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *                                                                           *
 *                          Constructores                                    *
 *                                                                           *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public Turno(int noTurno)
    {
        fechaInicio = Calendar.getInstance();
        setNoTurno(noTurno);
    }

    public void setNoTurno(int noTurno)
    {
        this.noTurno = noTurno;
    }

    public void setFechaFin()
    {
        fechaFin = Calendar.getInstance();
    }

    public void setVentanilla(int ventanilla)
    {
        this.ventanilla = ventanilla;
    }

    public int getNoTurno()
    {
        return this.noTurno;
    }

    public Calendar getFechaInicio()
    {
        return fechaInicio;
    }

    public Calendar getFechaFin()
    {
        setFechaFin();
        return fechaFin;
    }

    public int getVentanilla()
    {
        return ventanilla;
    }
}
