/*
 * Turnador: Conexion.java
 * Recibe y responde los mensajes a los clientes.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.servidor;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.EOFException;

/** Socket por unidad.
 * Se establece la conexión de cada una de las.*/
public class Conexion implements Runnable
{
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                      *
     *                      Atributos de la clase                           *
     *                                                                      *
     *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    private Socket socket; // Conexión directa al cliente
    private ServerSocket servidor; // Socket servidor
    private ObjectInputStream flujoEntrada; // Flujo de entrada de datos.
    private ObjectOutputStream flujoSalida; // Flujo de salida de datos.

    private LineaDeEspera lineaDeEspera; // Referencia a la clase que lo llama.
    private int noVentanilla; // Número de ventanilla u oficina.

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                      *
     *                       Operaciones de la clase                        *
     *                                                                      *
     *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**Espera conexión para despues procesarla.
     *
     * Una vez creado el objeto se espera la conexión a partir de este enlace.
     * @param servidor Socket servidor que recibe las conexiones.
     * @throws IOException Errores en Entrada/Salida. */
    public Conexion(ServerSocket servidor)
            throws IOException
    {
        this.servidor = servidor;
        socket = servidor.accept();
    }

    /** Prepara los flujos de entrada/salida.
     * Crea el flujo de entrada y lo vacía para evitar datos basura en la
     * comunicación. También crea el flujo de entrada que es el que va a
     * recibir los mensajes.
     * @throws IOException Errores en entrada y Salida. */
    public void obtenerFlujos() throws IOException
    {
        flujoSalida = new ObjectOutputStream(socket.getOutputStream());
        flujoSalida.flush();
        flujoEntrada = new ObjectInputStream(socket.getInputStream());
    }
    
    public void addOwner(LineaDeEspera lineaDeEspera)
    {
        this.lineaDeEspera = lineaDeEspera;
    }

    /**Procesa la conexión establecida.
     * Recibe el mensaje e informa al servidor. Si es la cadena de salida
     * "FINALIZADA" termina la conexión si es "FINALIZAR TODO" informa al
     * servidor que debe finalizar todas las conexiones.
     * @param socketServidor Se le informa que se hicieron actualizaciones.
     * @throws IOException Error de flujos.*/
    public String procesarMensajes()
        throws IOException, ClassNotFoundException
    {
        String mensaje = (String) flujoEntrada.readObject();
        if(mensaje.equals("GENERAR"))
        {
            int turnoGenerado = lineaDeEspera.generarTurno();
            enviarMensaje("" + turnoGenerado);
        }
        if(mensaje.length() > 5)
            if(mensaje.substring(0, 5).equals("NUEVO"))
            {
                int ventanilla = Integer.parseInt(mensaje.substring(6));
                int turnoleido = lineaDeEspera.obtenerTurno(ventanilla);
                if(turnoleido != -1)
                    lineaDeEspera.enviarMensajeMasivo(
                            lineaDeEspera.getNombre() + " " + ventanilla +
                            " TURNO " + turnoleido);
                else
                    enviarMensaje("NOHAYTURNOS");
            }
        return mensaje;
    }

    /**Envía mensajes al socket cliente.
     * Recibe un parámetro el cual usará para informar al cliente de algún dato
     * @param mensaje Cadena con el texto a enviar */
    public void enviarMensaje(String mensaje) 
    {
        try
        {
            flujoSalida.writeObject(mensaje);
            flujoSalida.flush();
        }
        catch(IOException io)
        {
            io.printStackTrace();
        }
    }

    /**Cierra la conexión.
     * Cierra todos los flujos. así como el socket que esta en uso.*/
    public void cerrarConexion()
    {
        try
        {
            flujoSalida.close();
            flujoEntrada.close();
            socket.close();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

   /*-----------------------------------------------------------------------*
    *                                                                       *
    *                          Funciones get y set                          *
    *                                                                       *
    *-----------------------------------------------------------------------*/
    public void setNoVentanilla(int noVentanilla)
    {
        this.noVentanilla = noVentanilla;
    }

    /**Subproceso paralelo del programa.
     * Ejecuta el procesamiento de los mensajes y procesa las excepciones
     * generadas. NOTA: Se recomienda redirigir la salida a algún archivo de
     * bitacora.*/
    public void run()
    {
        try
        {
            String mensaje;
            do
                mensaje = procesarMensajes();
            while(!mensaje.equals("FINALIZADA"));
        }
        catch(EOFException ex)
        {
            System.out.println("Conexión terminada");
        }
        catch(ClassNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            cerrarConexion();
            lineaDeEspera.eliminarConexion(this);
        }
    }
} // obtener de la clase
