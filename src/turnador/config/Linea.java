/*
 * Turnador: Linea.java
 * Subnodo del arbol sintáctico.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez
 *                                      <sistemas.will@alianzacpa.com.mx>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.config;

/**Almacena una de las hojas de la estructura de arbol del analizador
   sintáctico.*/
public class Linea{
    private int linea; //Indice de la línea.
    private int turno; // Turno inicial.
    private int ventanilla; // Ventanilla del turno.
    private String nombre; // Nombre del área de la línea de espera.

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *                                                                           *
 *                         Funciones Set y Get                               *
 *                                                                           *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*-----------------           Funciones Set       ---------------------------*/
    /**Establece el indice de la línea.
     * @param linea Indice de la línea.*/
    public void setLinea(int linea)
    {
        this.linea = linea;
    }

    /**Establece el turno inicial.
     * @param turno Turno inicial.*/
    public void setTurno(int turno)
    {
        if(turno >= 1000)
            turno = 0;
        this.turno = turno;
    }

    /**Establece el ventanilla del turno.
     * @param ventanilla Ventanilla del turno.*/
    public void setVentanilla(int ventanilla)
    {
        this.ventanilla = ventanilla;
    }

    /**Establece el nombre del área de la línea de espera.
     * @param nombre Nombre del área de la línea de espera.*/
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

/*-----------------           Funciones Get       ---------------------------*/
    /**Obtiene el indice de la línea.
     * @return Indice de la línea.*/
    public int getLinea()
    {
        return this.linea;
    }

    /**Obtiene el turno inicial.
     * @return Turno inicial.*/
    public int getTurno()
    {
        return this.turno;
    }

    /**Obtiene la ventanilla del turno.
     * @return Ventanilla del turno.*/
    public int getVentanilla()
    {
        return this.ventanilla;
    }

    /**Obtiene el nombre del área de la línea de espera.
     * @return Nombre del área de la línea de espera.*/
    public String getNombre()
    {
        return this.nombre;
    }
}
