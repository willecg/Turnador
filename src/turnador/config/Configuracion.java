/*
 * Turnador: Configuracion.java
 * Lee el archivo de configuración.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gómez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**Manejo del archivo de configuración.*/
public class Configuracion
{
  //  private Conectividad conectividad; // Items de conectividad.
  //  private Turnador turnador; // Items de datos de turnador.

    /* Componente Singleton. */
    private static Configuracion config = new Configuracion();

    private boolean abierto;

    private ArrayList<Arbol> tree;
    private Iterator<Arbol> iter;

    /* Constructor Singleton. */
    private Configuracion()
    {
        abierto = false;
    }

    /**Entrega una instancia singleton de la clase Configuración.
     * En este caso se debe usar si se instancia por primer vez.
     * @param archivo Archivo de configuración.
     * @throws
     * 	    ParserException, FileNotFoundException, FormatterClosedException.*/
    public static Configuracion getInstancia(String archivoURI)
    		throws ParserException, FileNotFoundException
    {
        if(!config.getAbierto())
        {
            File archivo = new File(archivoURI);
            if(archivo.exists())
                if(archivo.isFile())
                    if(archivo.canRead())
                    {
                        Scanner entrada = new Scanner(archivo);
                        config.setAbierto(true);
                        String cadena = "";
                        while(entrada.hasNext())
                            cadena = cadena + entrada.next();
                        entrada.close();
                        Parser analizer = new Parser(cadena);
                        config.setTree(analizer.getTree());
                    }
                    else
                        throw new
                            ParserException("No se puede leer el archivo.");
                else
                    throw new ParserException(
                        "La ruta corresponde a un directorio.");
            else
                throw new ParserException("No existe el archivo.");
        }
        return config;
    }

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *                                                                       *
     *                       Set and get functions.                          *
     *                                                                       *
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    /**Establece si el archivo de configuración ya ha sido establecido.
     * @param abierto True si ya se cargó el archivo Falso si no.*/
    public void setAbierto(boolean abierto)
    {
        if(!this.abierto)
            this.abierto = true;
    }

    /**Establece el arbol de configuración.
     * @param tree Nuevo arbol de configuración.*/
    public void setTree(ArrayList<Arbol> tree)
    {
    	this.tree = tree;
    	iter = this.tree.iterator();
    }

    /**Obtiene si el archivo de configuración ya ha sido establecido.
     * @return Verdadero si ya se cargó el archivo Falso si no se ha cargado*/
    public boolean getAbierto()
    {
        return abierto;
    }

    /**Retorna el siguiente valor del vector tree, o nulo si ya no hay.
     * @return El siguiente elemento del vector.*/
    public Arbol getNextTree()
    {
    	if(iter.hasNext())
    	    return iter.next();
    	return null;
    }
}
