/*
 * Turnador: Arbol.java
 * Nodo del arbol sintáctico.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez
 *                                      <sistemas.will@alianzacpa.com.mx>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.config;

import java.util.ArrayList;
import java.util.Iterator;

/**Representación de cada una de las ramas del árbol sintáctico.*/
public class Arbol{
    private int servidor_id; // Identificador de la rama del árbol.
    private String host; // Dirección IP o nombre del servidor.
    private int puerto; // Puerto de escucha de red.
    private int maximoconexiones; // Máximo de conexiones.
    private ArrayList<Linea> linea; // Arreglo de subramas del líneas.
    private Iterator<Linea> lineaIter; // Iterador de linea.

    /**Constructor.
     * Inicializa al objeto. */
    public Arbol()
    {
        this.linea = new ArrayList<Linea>();
    }

    /**Agrega un segmento de líneas al arbol
     * @param linea objeto Linea que se desea agregar.*/
    public void addLinea(Linea linea)
    {
        this.linea.add(linea);
    }

    /**Obtiene las líneas del arbol
     * @return Siguiente objeto Linea.*/
    public Iterator<Linea> getIterLinea()
    {   
        return linea.iterator();
    }

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *                                                                           *
 *                         Funciones Set y Get                               *
 *                                                                           *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*-----------------           Funciones Set       ---------------------------*/
    /**Establece el identificador del servidor.
     * @param servidor_id Identificador del servidor.*/
    public void setServidor_id(int servidor_id)
    {
        this.servidor_id = servidor_id;
    }

    /**Establece la dirección IP o nombre del servidor.
     * @param host Dirección IP o nombre del servidor.*/
    public void setHost(String host)
    {
        this.host = host;
    }

    /**Establece el puerto de escucha de red.
     * @param puerto Puerto de escucha de red.*/
    public void setPuerto(int puerto)
	{
	    this.puerto = puerto;
	}

    /**Establece el máximo de conexiones permitidas.
     * @param maximoconexiones Máximo de conexiones.*/
    public void setMaximoconexiones(int maximoconexiones)
    {
        this.maximoconexiones = maximoconexiones;
    }

/*-----------------           Funciones Get       ---------------------------*/
    /**Obtiene el identificador del servidor
     * @return Identificador de la rama del árbol.*/
    public int getServidor_id()
    {
        return this.servidor_id;
    }

    /**Obtiene el dirección IP o nombre del servidor.
     * @return Dirección IP o nombre del servidor.*/
    public String getHost()
    {
        return this.host;
    }

    /**Obtiene el puerto de escucha de red.
     * @return Puerto de escucha de red.*/
    public int getPuerto()
	{
	    return this.puerto;
	}

    /**Obtiene el máximo de conexiones.
     * @return Máximo de conexiones.*/
    public int getMaximoconexiones()
    {
        return this.maximoconexiones;
    }
}
