/*
 * Turnador: Parser.java
 * Analizador sintactico del archivo de configuración.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.config;

import java.util.ArrayList;
import java.util.Iterator;

/**Analizador sintáctico del archivo de configuración del turnador.
 * Una vez creado el objeto tipo Parser se debe llamar al método getTree() para
 * elaborar el árbol sintáctico y regresarlo como valor de retorno.*/
public class Parser
{
    private String cadena;
    private int indice;

    private final char tokenChars [] = {'{', '}', '=', ';'};
    private final String servidor = "servidor";
    private final String sTokens [] =
            {"host", "puerto", "maximoconexiones", "linea"};
    private final String lTokens [] =
            {"turno", "ventanilla", "nombre"};

    private ArrayList<Arbol> arboles;

	/**Constructor default.
	 * @param cadena Cadena de entrada que se va a parsear.*/
    Parser(String cadena)
    {
        setIndice(0);
        this.cadena = cadena;
    }

    /**Función principal genera arbol sintáctico.
     * Genera un árbol sintáctico con la información establecida en el archivo
     * de configuración, en caso de algún error, esta función termina la
     * ejecución del programa, bajo el supuesto de que si el archivo de
     * configuración esta mal no se debe de continuar con el programa.
     * @return arbol sintáctico completo.*/
    public ArrayList<Arbol> getTree() throws ParserException
    {
        arboles = new ArrayList<Arbol>();
        String aux;
        setIndice(0);
        while(getIndice() < cadena.length())
        {
            if(obtenerToken().equals("servidor"))
            {
                Arbol arbol = new Arbol();
                analizarSeparador(obtenerToken(), "=", "servidor");
                aux = obtenerToken();
                if(verificarEntero(aux) == 0)
                    arbol.setServidor_id(Integer.parseInt(aux));
                analizarSeparador(obtenerToken(), "{", "servidor");

                int s_token = 0;
                boolean lineaFlag = false;
                do{
                    aux = obtenerToken();
                    if(!aux.equals("}"))
                    {
                        s_token = verificarlToken(aux, sTokens);
                        analizarSeparador(obtenerToken(), "=", aux);
                        aux = obtenerToken();
                        switch(s_token)
                        {
                        case 0:
                            arbol.setHost(aux);
                            break;
                        case 1:
                            verificarEntero(aux);
                            arbol.setPuerto(Integer.parseInt(aux));
                            break;
                        case 2:
                            verificarEntero(aux);
                            arbol.setMaximoconexiones(Integer.parseInt(aux));
                            break;
                        case 3:
                            verificarEntero(aux);
                            Linea linea = new Linea();
                            linea.setLinea(Integer.parseInt(aux));
                            String laux = obtenerToken();
                            analizarSeparador(laux, "{", "linea");
                            lineaFlag = true;
                            do{
                                laux = obtenerToken();
                                if(!laux.equals("}"))
				                {
                                    int n_token = verificarlToken(
                                                            laux, lTokens);
                                    analizarSeparador(obtenerToken(),
                                                      "=",
                                                      lTokens[n_token]);
                                    laux = obtenerToken();
                                    switch(n_token)
                                    {
                                    case 0:
                                        verificarEntero(laux);
                                        linea.setTurno(Integer.parseInt(laux));
                                        break;
                                    case 1:
                                        verificarEntero(laux);
                                        linea.setVentanilla(
                                        	       Integer.parseInt(laux));
                                        break;
                                    case 2:
                                        linea.setNombre(laux);
                                    } // switch secundario
                                    analizarSeparador(obtenerToken(),
                                                      ";",
                                                      lTokens[n_token]);
                                }
                            }
                            while(!laux.equals("}") &&
                                  getIndice() < cadena.length());
                            analizarSeparador(laux, "}", "linea");
                            arbol.addLinea(linea);
                        } // switch principal
                        if(lineaFlag)
                            lineaFlag = false;
                        else
                            analizarSeparador(obtenerToken(),
                                              ";",
                                              sTokens[s_token]);
                    } // if(!aux.equals("}"))
                }
                while(!aux.equals("}") && getIndice() < cadena.length());
                arboles.add(arbol);
            } // If se encuentra un servidor
            else
                throw new ParserException("Falta token 'servidor'");
        } // While principal
        return arboles;
    } // Función

    /**Lee el siguiente token de la cadena de entrada.
     * @return Siguiente token, cadena vacía si ya no hay mas tokens.*/
    private String obtenerToken()
    {
        // Obtiene si es un caracter separador {}=;
        for(int i = 0;
                i < tokenChars.length &&
                getIndice() < cadena.length();
            i++)
            if(tokenChars[i] == cadena.charAt(getIndice()))
            {
                setIndice(getIndice() + 1);
                return "" + tokenChars[i];
            }

        // Obtiene si es un token que no sea separador.
        String token = "";
        boolean tokenCompleto = false;
        while(getIndice() < cadena.length() && !tokenCompleto)
        {
            for(int i = 0;
                i < tokenChars.length &&
                !tokenCompleto &&
                getIndice() < cadena.length();
            i++)
                if(tokenChars[i] == cadena.charAt(getIndice()))
                {
                    tokenCompleto = true;
                    break;
                }
            if(!tokenCompleto)
            {
                token += cadena.charAt(getIndice());
                setIndice(getIndice() + 1);
            }
        }
        return token;
    }

    /**Compara si dos token son iguales, de no serlo imprime error
     * @param separador Cadena con el token que se desea comprobar.
     * @param esperado Cadena con el token de referencia.
     * @param tokenPrevio Referencia sobre la que se muestra el error.
     * @return 0 Si los token son iguales, 1 si son diferentes.*/
    public static int
      analizarSeparador(String separador, String esperado, String tokenPrevio)
      throws ParserException
    {
        if(!separador.equals(esperado))
            throw new ParserException("Caracter '" + esperado +
                                       "' perdido en '" + tokenPrevio + "'");
	    return 0;
    }

    /**Verifica si un String puede representar un entero.
     * @param valor Cadena a probar.
     * @return 0 si la cadena puede representar un entero de lo contrario 1.*/
    public static int verificarEntero(String valor) throws ParserException
    {
        for(int i = 0; i < valor.length(); i++)
            if(valor.charAt(i) < '0' || valor.charAt(i) > '9')
                throw new ParserException("El token '" + valor +
                            		"'no corresponde a un número entero.");
        return 0;
    }

    /**Verifica si ún token esta en el arregle de tokens que entran.
     * @param token Cadena a probar.
     * @param listaTokens[] Lista de cadena desde donde se desea buscar.
     * @return -1 si no se encontró la cadena. Si se encuentra se regresa el
               índice donde está la cadena.*/
    public static int
    verificarlToken(String token, String listaTokens[]) throws ParserException
    {
        for(int i = 0; i < listaTokens.length; i++)
            if(token.equals(listaTokens[i]))
                return i;
        throw new ParserException("Token inesperado '" + token + "'");
    }

    /**Establece el índice actual.
     * @param indice Nueva posición del apuntador a la cadena.*/
    private void setIndice(int indice)
    {
        this.indice = indice;
    }

    /**Obtiene el índice actual.
     * @return Posición del apuntador a la cadena.*/
    private int getIndice()
    {
        return this.indice;
    }
}
