/*
 * Turnador: ParserException.java
 * Exepciones generadas desde el parser.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador.config;

/**Manejador de excepciones del parser del archivo de configuración.*/
public class ParserException extends Exception
{
    private static final long serialVersionUID = -7034897190745766941L;
    ParserException()
    {
        this("Parser");
    }
    ParserException(String cause)
    {
        super(cause);
    }
}
