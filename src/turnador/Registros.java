/*
 * Turnador: Registros.java
 * Manipula archivos de bitacoras y registra en ellos.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gomez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package turnador;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**Manipula los archivos de registro o de bitacora del programa,
 * utilizando el método registrarBitacora para indicar la fecha y hora de la
 * actividad y un mensaje con el registro solicitado.*/
public class Registros
{
    private FileWriter log;
    private PrintWriter logWriter;
    Calendar fecha;
    public final static SimpleDateFormat formato = 
        new SimpleDateFormat("d/M/y-H:m:s|");

    /**Crea el objeto de tipo Registros.
     * Para ello utiliza el archivo indicado en el parámetro.
     * @param logDir Ruta al archivo y nombre incluido del archivo de bitacora.
     * @throws FileNotFoundException Por si el archivo no se encontrara.
     * @throws IOException Error de lectura o escritura.*/
    public Registros(String logDir) throws FileNotFoundException, IOException
    {
        log = new FileWriter(logDir, true);
        logWriter = new PrintWriter(log);
        fecha = Calendar.getInstance();
    }
    
    /**Agrega un nuevo registro a la bitácora.
     * Toma el mensaje que se recibe como parámetro y le agrega la fecha y la
     * hora para tener una referencia, a continuación escribe esta información
     * en el archivo de bitácora y vacía el bufer de escritura.
     * @param mensaje Mensaje o registro que se quiere escribir al archivo.*/
    public void registrarBitacora(String mensaje, boolean con_fecha)
    {
        if(con_fecha)
            logWriter.println(formato.format(
                        Calendar.getInstance().getTime()) + mensaje);
        else
            logWriter.println(mensaje);
        logWriter.flush();
    }

    /**Cierra archivos.
     * Libera los recursos que requiere la clase, entre ellos el acceso a los
     * archivos.
     * @throws IOException Error al intentar cerrar los archivos.*/
    public void cerrar()throws IOException
    {
        log.close();
        logWriter.close();
    }
}
