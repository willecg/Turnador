/*
 * Turnador: Turnador.java
 * Inicio del servidor.
 *
 * Copyright © 2015 William Ernesto Cárdenas Gómez <willecg@openmailbox.org>
 * Copyright © 2015 Departamento de Sistemas Caja Popular Apaseo el Alto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.SecurityException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.io.IOException;

import turnador.config.Configuracion;
import turnador.config.Arbol;
import turnador.config.ParserException;
import turnador.servidor.ServidorTurnos;


public class Turnador
{
	/* Para modificar las ubicaciones de los archivos se utilizan los
     * argumentos la línea de ordenes.
     * -lc <carpeta de bitácoras>
     * -log <Bitácora general del sistema>
     * -qlog <carpeta de bitacorad de tiempos de cada línea de espera>
     * -conf <Archivo de configuración>
     * -help
     * -show
     * -term */
    public static void main(String[] args)
    {
        String carpeta_log = "/var/log/turnadores/";
        String configFile = "/etc/Turnador/turnador.conf";

        final String[] opts =
                {"-log","-conf", "-help", "-show", "-term"};
        boolean terminar = false;
        boolean flag;
        for(int i = 0; i < args.length; i++)
        {
            flag = true;
            if(args[i].equals(opts[0]))
            {
                carpeta_log = args[++i];
                flag = false;
            }
            if(args[i].equals(opts[1]))
            {
                configFile = args[++i];
                flag = false;
            }
            if(args[i].equals(opts[2]))
            {
                Turnador.help();
                System.exit(0);
            }
            if(args[i].equals(opts[3]))
            {
                Turnador.show();
                System.exit(0);
            }
            if(args[i].equals(opts[4]))
            {
                terminar = true;
                flag = false;
            }
            if(flag)
            {
                System.out.println("Opción incorrecta '" + args[i] +"'");
                Turnador.help();
                System.exit(1);
            }
        } // Fin del for.

        try
        {
            /* Ubicaciones predeterminadas para bitácora y configuración */
            final String logfile = "Turnador.log";
            final String queues_log = "queues/";
            Turnador.dirsAndFiles(carpeta_log, false);
            Turnador.dirsAndFiles(carpeta_log + logfile, true);
            Turnador.dirsAndFiles(carpeta_log + queues_log, false);
            Turnador.dirsAndFiles(configFile, true);

            Configuracion config = Configuracion.getInstancia(configFile);
            if(!terminar)
            {
                ServidorTurnos servidor =
                    new ServidorTurnos(config.getNextTree(), carpeta_log);
                servidor.detener();
            }
            else
                ServidorTurnos.apagarServidor(config.getNextTree());
        }
    	catch(ParserException parserEx)
    	{
    	    System.out.println(parserEx.getMessage());
    	    parserEx.printStackTrace();
    	}
        catch(FileNotFoundException fnfe)
    	{
    	    System.out.println(fnfe.getMessage());
    	    fnfe.printStackTrace();
    	}
        catch(IOException io)
        {
	    System.out.println(io.getMessage());
    	    io.printStackTrace();
        }
        catch(ClassNotFoundException cnfe)
        {
	    System.out.println(cnfe.getMessage());
    	    cnfe.printStackTrace();
        }
    }

    /**Muestra la ayuda general.*/
    public static void help()
    {
        System.out.println(
"Opciones de arranque del servidor.\n\n" +

" -log <carpeta>       Carpeta para almacenar las bitácoras.\n" +
" -conf <Archivo conf> Indica el archivo de configuración del servidor\n\n" +
" -help                Muestra este mensaje de ayuda.\n" +
" -show                Muestra la licencia y los derechos de autor.\n" +
" -term                Envía señal de terminación. Cierra el servidor."
        );
    }

    /**Imprime un mensaje referente a la licencia.*/
    public static void show()
    {
        System.out.println(
"\n                            < < T U R N A D O R > >\n\n" +
"Copyright (C) 2015 William Ernesto Cárdenas Gómez <willecg@openmailbox.org>\n"
+
"Copyright (C) 2015 Departamento de Sistemas Caja Popular Apaseo el Alto.\n\n"
+
"Turnador es software libre, puede redistribuirlo y/o modificarlo\n"
+
"bajo los términos de la Licencia Pública General de GNU tal como\n" +
"la publica la Free Software Foundation, tanto en su versión 3 de la\n" +
"Licencia como (a su elección) cualquier versión posterior.\n\n" +

"Turnador se distribuye con la esperanza de que se sea útil,pero\n" +
"SIN NINGUNA GARANTÍA; sin incluso la garantía implícita de\n" +
"MERCANTILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.\n" +
"Consulte la Licencia Pública General de GNU para obtener más detalles.\n\n" +

"Debería haber recibido una copia de la Licencia Pública General de GNU\n" +
"junto con este programa. Si no es así, escriba a la Free Software " +
"Foundation,\n\n" +
"Inc51 Franklin Street , Fifth Floor, Boston,\n" +
"MA 02110-1301, USA."
            );
    }

    /**Comprueba la existencia del archivo o directorio solicitado
     * si no existe lo crea, de lo contrario lo deja como esta.
     * @param address Nombre del archivo a revisar.
     * @param file 'true' si es archivo 'false' si es directorio.
     * @throws FormatterClosedException, FileNotFoundException. */
    public static void dirsAndFiles(String address, boolean esArchivo)
                        throws FormatterClosedException,
                               FileNotFoundException
    {
    	File archivo = new File(address);
    	if(!archivo.exists())
    	{
            if(esArchivo)
            {
                Formatter farchivo = new Formatter(address);
                farchivo.close();
            }
            else
                archivo.mkdir();
        }
    }
}
